const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports={
	entry:['./src/main'],
	output:{
		path:path.join(__dirname,'dist'),
		filename:'bundle.js'
	},
	devServer: {
		historyApiFallback:true,
		hot:true
	},
	resolve:{
		alias:{
			'vue$':'vue/dist/vue.esm.js',
			'vis$':'vis/dist'
		}
	},
	plugins:[
		new HtmlWebpackPlugin({
			template:'./index.tpl.html'
		})
	],
	module:{
		loaders:[
			{
				test:/\.css$/,
				loaders:['style-loader','css-loader']
			},
			{
				test:/\.js$/,
				exclude:/(node_modules)/,
				use : [{
					loader: "babel-loader",
					options: {
						presets: ['env']
					}
				}]
			},
			{
				test:/\.vue$/,
				use:[
					{
						loader: "vue-loader",
						options: {

						}
					}
				]
			},
			{
				test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
				loader: 'url-loader?limit=1024'
			}
		]
	}
};